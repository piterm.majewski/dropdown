package pl.pmajewski.dropgame.service.platform;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.LinkedList;

import pl.pmajewski.dropgame.model.PlatformLevel;
import pl.pmajewski.dropgame.model.PositionListener;
import pl.pmajewski.dropgame.model.PositionSource;
import pl.pmajewski.dropgame.model.WorldWrapper;
import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class PlatformManager implements PositionListener {

    private LinkedList<PlatformLevel> platforms = new LinkedList<>();

    private Camera camera;
    private WorldWrapper world;
    private Stage stage;

    public PlatformManager(Camera camera, WorldWrapper world, Stage stage) {
        this.camera = camera;
        this.world = world;
        this.stage = stage;
    }

    @Override
    public void notify(PositionSource source, Vector2 position) {
        if(platforms.isEmpty()) {
            generatePlatform(position.y);
        }

        while (!platforms.isEmpty()
                && platforms.getLast().getPositionY() > cameraBottomLine()) {
            generatePlatform(platforms.getLast().getPositionY());
        }
    }

    private void generatePlatform(Float lastPosition) {
        DebugLogger.debug("Generate Platform", "PLATFORM_MGR");
    }

    private Float cameraBottomLine() {
        return camera.position.y - GlobalProperties.APP_HEIGHT / 2;
    }

    public void draw(Batch batch, float parentAlpha) {
        for (PlatformLevel temp: platforms) {
            temp.draw(batch, parentAlpha);
        }
    }
}
