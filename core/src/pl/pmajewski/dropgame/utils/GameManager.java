package pl.pmajewski.dropgame.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

import lombok.Getter;
import lombok.Setter;
import pl.pmajewski.dropgame.GameMain;

public class GameManager {

    private static GameManager instance;

    @Getter
    @Setter
    private GameMain gameInstance;
    @Getter
    private AssetManager assetManager;

    private GameManager() {
        assets();
    }

    private void assets() {
        assetManager = new AssetManager();
        assetManager.load("jump.png", Texture.class);
        assetManager.finishLoading();
        DebugLogger.debug("Assets loaded", "[GAME UTILS] [ASSETS]");
    }

    public static GameManager getInstance() {
        if(instance == null) {
            synchronized (GameManager.class) {
                if(instance == null) {
                    instance = new GameManager();
                }
            }
        }

        return instance;
    }
}
