package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.graphics.g2d.Batch;

import java.util.Arrays;
import java.util.List;

public class PlatformLevel {

    private List<Platform> platforms;
    private Float positionY;

    PlatformLevel(Platform... platforms) {
        this.platforms = Arrays.asList(platforms);
        positionY = platforms[0].getBodyPosition().y;
    }

    public void draw(final Batch batch, final Float alpha) {
        for (Platform temp : platforms) {
            temp.draw(batch, alpha);
        }
    }

    public Float getPositionY() {
        return positionY;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }
}
