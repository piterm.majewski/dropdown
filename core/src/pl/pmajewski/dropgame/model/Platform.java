package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GameManager;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class Platform extends GameActor {

    private TextureRegion textureRegion;
    private Vector2 size;

    public Platform(World world, Vector2 position, Vector2 size) {
        super(world);
        this.size = size;
        createBody(world, position);
        setUpTexture();
    }

    private void createBody(World world, Vector2 position) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(position);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(size.x, size.y);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.PLATFORM_DENSITY;
        fixtureDef.restitution = 1f;
        fixtureDef.friction = 0f;

        Body body = world.createBody(bodyDef);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(this);
        body.setUserData(this);

        shape.dispose();
        this.body = body;
    }

    private void setUpTexture() {
        Texture texture = GameManager.getInstance().getAssetManager()
                .get(GlobalProperties.PLATFORM_TEXTURE, Texture.class);
        this.textureRegion = new TextureRegion(texture);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        DebugLogger.debug(500l,"bodyPosition="+body.getPosition()+" angle="+body.getAngle(), "PLATFORM");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        drawHelper(batch, textureRegion);
    }

    @Override
    protected Float getBodyWidth() {
        return size.x;
    }

    @Override
    protected Float getBodyHeight() {
        return size.y;
    }
}
