package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.math.Vector2;

public interface PositionListener {

    void notify(PositionSource source, Vector2 position);
}
