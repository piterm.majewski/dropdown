package pl.pmajewski.dropgame.model.factory;

import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.dropgame.model.BallModel;
import pl.pmajewski.dropgame.model.GameActor;
import pl.pmajewski.dropgame.model.WorldWrapper;
import pl.pmajewski.dropgame.stage.GameStage;

public class ActorFactory {

    public enum Actor { MAIN }

    public GameStage stage;

    public ActorFactory(GameStage stage) {
        this.stage = stage;
    }

    public GameActor create(Actor actor, WorldWrapper worldWrapper) {

        switch (actor) {
            case MAIN:
                return createMain(worldWrapper.getWorld());
            default:
                return null;
        }
    }

    private GameActor createMain(World world) {
        BallModel model = new BallModel(world);
        model.addPositionListener(stage);
        return model;
    }
}
