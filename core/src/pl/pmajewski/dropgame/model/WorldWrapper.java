package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.dropgame.service.WorldContactListener;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class WorldWrapper {

    private World world;
    private float accumulator = 0f;

    public WorldWrapper() {
        this.world = new World(GlobalProperties.WORLD_GRAVITY, true);
        world.setContactListener(new WorldContactListener());
        World.setVelocityThreshold(0f);
    }

    public void act(float delta) {
        this.accumulator += delta;
        if(accumulator >= delta) {
            world.step(GlobalProperties.TIME_STEP, 6, 2);
            this.accumulator -= GlobalProperties.TIME_STEP;
        }
    }

    public Vector2 mapScreenToWorld(Vector2 screenPosition) {
        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();

        float worldX = GlobalProperties.APP_WIDTH*screenPosition.x/screenWidth;
        float worldY = GlobalProperties.APP_HEIGHT*screenPosition.y/screenHeight;

        return new Vector2(worldX, worldY);
    }

    public World getWorld() {
        return world;
    }
}
