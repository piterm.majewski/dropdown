package pl.pmajewski.dropgame.stage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;

import lombok.Getter;
import pl.pmajewski.dropgame.model.BallModel;
import pl.pmajewski.dropgame.model.PositionListener;
import pl.pmajewski.dropgame.model.PositionSource;
import pl.pmajewski.dropgame.model.WorldWrapper;
import pl.pmajewski.dropgame.model.factory.ActorFactory;
import pl.pmajewski.dropgame.service.platform.PlatformManager;
import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class GameStage extends Stage implements PositionListener {

    @Getter
    private WorldWrapper worldWrapper;
    @Getter
    private Box2DDebugRenderer debugRender;
    private ActorFactory actorFactory;
    private PlatformManager platformManager;
    private BallModel ball;

    public GameStage() {
        super(new FillViewport(GlobalProperties.APP_WIDTH, GlobalProperties.APP_HEIGHT, new OrthographicCamera(GlobalProperties.APP_WIDTH, GlobalProperties.APP_HEIGHT)));
        Gdx.input.setInputProcessor(this);
        this.worldWrapper = new WorldWrapper();

        debugRender = new Box2DDebugRenderer(true, true, true, true, true, true);
        actorFactory = new ActorFactory(this);

        ball = (BallModel) actorFactory.create(ActorFactory.Actor.MAIN, worldWrapper);
        addActor(ball);

        platformManager = new PlatformManager(getCamera(), worldWrapper, this);
        ball.addPositionListener(platformManager);
    }


    private void setCameraPosition() {
        getCamera().position.y = ball.getY();
    }

    @Override
    public void act(float delta) {
        setCameraPosition();
        super.act(delta);

        worldWrapper.act(delta);
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(Input.Keys.RIGHT == keyCode) {
            ball.forceRight();
        }

        if (Input.Keys.LEFT == keyCode) {
            ball.forceLeft();
        }

        return super.keyDown(keyCode);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
//        DebugLogger.debug("x="+screenX+" y="+screenY+" pointer="+pointer+" button="+button, "STAGE", "TOUCH");
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector2 touch = new Vector2(screenX, screenY);
        Vector2 worldPosition = worldWrapper.mapScreenToWorld(touch);
        DebugLogger.debug(250l,"touch="+touch+" world="+worldPosition, "STAGE", "TOUCH");
        ball.moveX(worldPosition.x);
        return super.touchDragged(screenX, screenY, pointer);
    }

    public Box2DDebugRenderer getBox2DDebug() {
        return this.debugRender;
    }

    public World getWorld() {
        return this.worldWrapper.getWorld();
    }

    @Override
    public void notify(PositionSource source, Vector2 position) {

    }
}
